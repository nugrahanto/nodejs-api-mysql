var db = require('../dbconnection');

var soal = {
	getAllSoal:function(id_mapel, callback){
		return db.query("select * from soal where id_mapel=?",[id_mapel],callback);
	},

	getSoalbyId:function(params, callback){
		return db.query("select * from soal where id_mapel=? AND id_soal=?",[params.mapel,params.soal],callback);
	},

	addSoal:function(body, callback){
		db.getConnection(function(err, connection){
			connection.beginTransaction(function(err){
				if(err) { 
					return err;
					connection.rollback(function(){
						connection.release();
					});
				}
				else {
					connection.query("insert into soal(id_mapel,soal,gambar,jawaban_benar) values(?,?,?,?)",
					[body.id_mapel,body.soal,body.gambar,body.jawaban_benar],
					function(err,results)
					{
					 	if(err)
					 	{
					 		return callback(err);
					 		connection.rollback(function(){
					 			connection.release();
					 		});
					 	}
					 	else
					 	{
					 		array = body.jawaban;
							array = array.split(",");
							values = []

							for(var i=0; i< array.length; i++){
								values.push([results.insertId,array[i]]);
							}

							connection.query("insert into jawaban(id_soal,jawaban) values ?",
							[values],function(err,results){
								if(err)
								{
									connection.rollback(function(){
					 					connection.release();
					 					return callback(err);
					 				});
								}
								else
								{
									connection.commit(function(err){
					 					if(err)
					 					{
					 						connection.rollback(function(){
					 							connection.release();
					 							return callback(err);
					 						});
					 					}
					 					else
					 					{
					 						return callback(results);
					 					}
					 				});
								}
							});
					 	}
					});
				}
			});
		});
	},
/*
	updateSoal:function(id, body, callback){
		db.getConnection(function(err, connection){
			connection.beginTransaction(function(err){
				if(err) { 
					return err;
					connection.rollback(function(){
						connection.release();
					});
				}
				else {
					connection.query("update soal(id_mapel,soal,gambar,jawaban_benar) values(?,?,?,?) where id_soal=?",
					[body.id_mapel,body.soal,body.gambar,body.jawaban_benar,id],
					function(err,results)
					{
					 	if(err)
					 	{
					 		return callback(err);
					 		connection.rollback(function(){
					 			connection.release();
					 		});
					 	}
					 	else
					 	{
					 		array = body.jawaban;
							array = array.split(",");
							values = []

							for(var i=0; i< array.length; i++){
								values.push([array[i]]);
							}

							connection.query("update jawaban(jawaban) values ? where id_soal=?",
							[values, id],function(err,results){
								if(err)
								{
									connection.rollback(function(){
					 					connection.release();
					 					return callback(err);
					 				});
								}
								else
								{
									connection.commit(function(err){
					 					if(err)
					 					{
					 						connection.rollback(function(){
					 							connection.release();
					 							return callback(err);
					 						});
					 					}
					 					else
					 					{
					 						return callback(results);
					 					}
					 				});
								}
							});
					 	}
					});
				}
			});
		});
	},*/
};

module.exports = soal;