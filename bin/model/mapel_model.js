var db = require('../dbconnection');

var mapel = {
	getAllMapel:function(callback){
		//console.log(id);
		return db.query("select * from mapel",callback);
	},

	getMapelbyBidang:function(id_bidang, callback){
		return db.query("select * from mapel where id_bidang=?",[id_bidang],callback);
	},

	getMapelbyId:function(id_mapel, callback){
		return db.query("select * from mapel where id_mapel=?",[id_mapel],callback);
		console.log();
	},

	addMapel:function(body, callback){
		return db.query("insert into mapel values(?,?,?)",[body.id_mapel,body.id_bidang,body.mapel],callback);
	},

	updateMapel:function(id_mapel, values, callback){
		return db.query("update mapel set id_bidang=?,mapel=? where id_mapel=?",[values.id_bidang,values.mapel,id_mapel],callback);
	},

	deleteMapel:function(id_mapel, callback){
		return db.query("delete from mapel where id_mapel=?",[id_mapel],callback);
	}
};

module.exports = mapel;