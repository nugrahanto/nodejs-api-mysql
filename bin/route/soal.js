var express = require('express');
var router = express.Router();
var soal = require('../model/soal_model');
var array;

//get All soal, need variable "id mata pelajaran"
router.get('/:mapel', function(req,res,next){
	soal.getAllSoal(req.params.mapel, function(err,rows){
		if(err)
		{
			res.json(err)
		}
		else
		{
			res.json(rows)
		}
	})
});

//get soal by Id, need variable "id mata pelajaran & soal"
router.get('/id/:mapel&:soal?', function(req,res,next){
	soal.getSoalbyId(req.params, function(err,rows){
		if(err)
		{
			res.json(err)
		}
		else
		{
			res.json(rows)
		}
	})
});

//insert soal
router.post('/',function(req,res,next){
	soal.addSoal(req.body,function(err,count){
		if(err)
		{
			res.json(err);
		}
		else{
			res.json(req.body);
		}
	})
});

//update soal
router.put('/:id',function(req,res,next){
	soal.updateSoal(req.params.id,req.body,function(err,rows){
		if(err)
		{
			res.json(err);
		}
		else
		{
			res.json(rows);
		}
	})
});


module.exports = router;