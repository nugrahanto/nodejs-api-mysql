var express = require('express');
var router = express.Router();
var bidangstudi = require('../model/studi_model');

//get All bidang studi list
router.get('/', function(req,res,next){
	bidangstudi.getAllBidang(function(err,rows){
		if(err)
		{
			res.json(err)
		}
		else
		{
			res.json(rows)
		}
	})
});

//get bidang studi by Id, need variable "id bidang studi"
router.get('/id/:id?', function(req,res,next){
	bidangstudi.getBidangbyId(req.params.id, function(err,rows){
		if(err)
		{
			res.json(err)
		}
		else
		{
			res.json(rows)
		}
	})
});

module.exports = router;